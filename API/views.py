from django.db.models          import Avg
from rest_framework.response   import Response
from rest_framework.decorators import api_view
from .models                   import Register
from .serializer               import RegisterSerializer

@api_view(['POST'])
def addRegister(request):
    try:
        data  = request.data
        lista = data['sensor_data'] # Podria usar list(data.keys())[0] para obtener la key en caso de que varie.. por seguridad y para evitar posteos basura dejo la presentada en la cosigna

        for medicion in lista:
            Register.objects.create(value = float(medicion))
    except:
        return Response({"success": "false"})

    return Response({"success":"true"}) 

@api_view(['GET'])
def max(request):
    try:
        max = Register.objects.order_by('-value').first()
        return Response({"max": max.value})
    except:
        return Response({"success": "false"})

@api_view(['GET'])
def min(request):
    try:
        min = Register.objects.order_by('-value').last()
        return Response({"min": min.value})
    except:
        return Response({"success": "false"})


@api_view(['GET'])
def avg(request):
    try:
        value = Register.objects.aggregate(Avg('value'))["value__avg"]
        if(value == None):
            # Utilizo None ya que el promedio me podria dar cero.. solamente activo esta opcion cuando no poseo registros  
            return Response({"avg": "not enough registers"})
        else:
            return Response({"avg": round(value,2)}) # Redondeo ya que no tiene sentido tener muchos decimales en un caso de ejemplo
    except:
        return Response({"success": "false"})


# Extra orientativo para pruebas
@api_view(['GET'])
def all(request):
    try:
        mediciones = Register.objects.all()
        serializer = RegisterSerializer(mediciones, many=True)
        return Response(serializer.data) 
    except:
        return Response({"success": "false"})



    
