from django.urls import path
from . import views

urlpatterns = [
                path('save/'     , views.addRegister),
                path('get/min/'  , views.min),
                path('get/max/'  , views.max),
                path('get/avg/'  , views.avg),
                path('get/show/' , views.all),
              ]