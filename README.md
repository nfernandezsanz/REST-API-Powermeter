# REST-API-Powermeter
Simple REST API developed with Django and DRF.

<h2>Installation</h2>
<ul>
<li>Install Python 3.8.10 and PIP </li>
<li>Download the repository and go to the project location</li>
<li>pip install -r requirements.txt</li>
<li>python manage.py runserver</li>
</ul>

<h2>Example commands</h2>
<ul>
<li> curl http://localhost:8000/api/get/max/</li>
<li> curl http://localhost:8000/api/get/min/</li>
<li> curl http://localhost:8000/api/get/avg/</li>
<li> curl http://localhost:8000/api/get/show/</li>
<li>curl -X POST http://localhost:8000/api/save/ -d '{"sensor_data": [1, -2, 3.2, 7]}' -H "Content-Type:
application/json"</li>
</ul>



